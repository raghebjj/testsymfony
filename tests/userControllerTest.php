<?php

namespace App\Tests\Controller;

use App\Controller\UserController;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserControllerTest extends TestCase
{
    private $passwordEncoder;
    private $jwtManager;

    protected function setUp(): void
    {
        $this->passwordEncoder = $this->createMock(UserPasswordHasherInterface::class);
        $this->jwtManager = $this->createMock(JWTTokenManagerInterface::class);
    }

    public function testUserCreate(): void
    {
        $requestContent = json_encode([
            'email' => 'ragheb@jellad.com',
            'password' => 'test_password',
            'role' => ['ROLE_USER'],
            'tasks' => ['Task 1', 'Task 2'],
        ]);

        $request = Request::create('/api/createUser', 'POST', [], [], [], [], $requestContent);

        $userRepository = $this->createMock(UserRepository::class);
        $userRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $encodedPassword = 'encoded_password';

        $this->passwordEncoder->expects($this->once())
            ->willReturn($encodedPassword);

        $token = 'test_token';

        $this->jwtManager->expects($this->once())
            ->method('create')
            ->willReturn($token);

        $userController = new UserController($this->passwordEncoder, $this->jwtManager, $userRepository);
        $response = $userController->userCreate($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $responseData = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('token', $responseData);
        $this->assertEquals($token, $responseData['token']);
    }
}
