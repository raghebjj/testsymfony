Installation
Clonez le projet à partir du dépôt GitLab :


`git clone https://gitlab.com/raghebjj/testsymfony.git`
Accédez au répertoire du projet :
cd testsymfony
Installez les dépendances du projet à l'aide de Composer :

composer install
Configurez les paramètres de connexion à la base de données en copiant le fichier .env :

Modifiez ensuite le fichier .env pour fournir les informations de connexion appropriées à votre base de données.

Générez les clés SSH pour JWT en exécutant la commande suivante :


mkdir -p config/jwt
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout

Vous serez invité à fournir une passphrase pour la clé privée. Assurez-vous de conserver cette passphrase en lieu sûr.
Mettez à jour le schéma de base de données en exécutant la commande suivante :


php bin/console doctrine:schema:update --force
Cela créera les tables nécessaires dans la base de données.

Lancez le serveur de développement Symfony :


symfony serve
Le serveur sera accessible à l'adresse http://localhost:8000.

Routes et paramètres
Le projet "testsymfony" expose les routes suivantes :

1. POST /api/createUser
Crée un nouvel utilisateur.

Paramètres :
email (string, requis) : l'adresse e-mail de l'utilisateur.
password (string, requis) : le mot de passe de l'utilisateur.
role (array, requis) : le rôle de l'utilisateur (ex: ["ROLE_USER"]).
tasks (array, facultatif) : les tâches de l'utilisateur.
2. GET /api/users
Récupère la liste des utilisateurs avec leurs tâches associées.

Paramètres : aucun.
