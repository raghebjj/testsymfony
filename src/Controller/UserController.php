<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserController extends AbstractController
{
    private $passwordEncoder;
    private $jwtManager;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, JWTTokenManagerInterface $jwtManager, UserRepository $user)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->jwtManager = $jwtManager;
    }
    /**
     * @Route("/api/users", name="api_users", methods={"GET"})
     */
    public function getUsers(): JsonResponse
    {
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $users = $userRepository->findAll();
        $responseData = [];
        foreach ($users as $user) {
            $userTasks = [];
            foreach ($user->getTasks() as $task) {
                $userTasks[] = [
                    'id' => $task->getId(),
                    'title' => $task->getName(),
                ];
            }
            $responseData[] = [
                'id' => $user->getId(),
                'username' => $user->getUsername(),
                'email' => $user->getEmail(),
                'tasks' => $userTasks,
            ];
        }

        return new JsonResponse(['users' => $responseData]);
    }
    /**
     * @Route("/api/createUser", name="user_create", methods={"POST"})
     */
    public function userCreate(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $email = $data['email'];
        $password = $data['password'];

        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $emailExist = $userRepository->findOneBy(['email' => $email]);

        if ($emailExist) {
            return new JsonResponse([
                'status' => false,
                'message' => 'Cet Email existe déjà'
            ]);
        } else {
            $user = new User();
            $user->setEmail($email);
            $user->setRoles($data['role']);

            $encodedPassword = $this->passwordEncoder->encodePassword($user, $password);
            $user->setPassword($encodedPassword);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);

            if (isset($data['tasks'])) {
                foreach ($data['tasks'] as $taskData) {
                    $task = new Task();
                    $task->setName($taskData);
                    $task->setUser($user);
                    $entityManager->persist($task);
                }
            }
            $entityManager->flush();

            $token = $this->jwtManager->create($user);

            return new JsonResponse(['token' => $token], JsonResponse::HTTP_CREATED);
        }
    }
}
